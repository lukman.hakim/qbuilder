package com.qbuilder.core;

import jakarta.persistence.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Slf4j
public class CommonRepository {

    @PersistenceContext
    private final EntityManager entityManager;

    public <T> TypedQuery<T> find(SearchQuery searchQuery, Class<T> toClass) {
        return find(SearchBuilder.query(searchQuery), Pageable.unpaged(), toClass, toClass);
    }

    public <T> TypedQuery<T> find(SearchBuilder searchBuilder, Class<T> toClass) {
        return find(searchBuilder, Pageable.unpaged(), toClass, toClass);
    }

    public <T> TypedQuery<T> find(SearchQuery searchQuery, Class<?> sourceClass, Class<T> toClass) {
        return find(SearchBuilder.query(searchQuery), Pageable.unpaged(), sourceClass, toClass);
    }

    public <T> TypedQuery<T> find(SearchBuilder searchBuilder, Class<?> sourceClass, Class<T> toClass) {
        return find(searchBuilder, Pageable.unpaged(), sourceClass, toClass);
    }

    public <T> TypedQuery<T> find(SearchBuilder searchBuilder, Pageable pageable, Class<?> sourceClass, Class<T> toClass) {
        SearchBuilder sb = SearchBuilder.query(SearchQuery.copy(searchBuilder.getSearchQuery()));
        sb.setSourceClass(sourceClass);
        sb.build(pageable);

        String stringQuery = sb.getQuery();
        Map<String, Object> parameters = sb.getParameters();
        TypedQuery<T> query = entityManager.createQuery(stringQuery, toClass);

        for (String key : parameters.keySet()) {
            query.setParameter(key, parameters.get(key));
        }

        if (sb.getPageable().isPaged()) {
            query.setFirstResult((int) sb.getPageable().getOffset());
            query.setMaxResults(sb.getPageable().getPageSize());
        }

        return query;
    }

    public <T> T findOne(SearchQuery searchQuery, Class<T> toClass) {
        return findOne(SearchBuilder.query(searchQuery), toClass, toClass);
    }

    public <T> T findOne(SearchBuilder searchBuilder, Class<T> toClass) {
        return findOne(searchBuilder, toClass, toClass);
    }

    public <T> T findOne(SearchQuery searchQuery, Class<?> sourceClass, Class<T> toClass) {
        return findOneOptional(SearchBuilder.query(searchQuery), sourceClass, toClass).orElse(null);
    }

    public <T> T findOne(SearchBuilder searchBuilder, Class<?> sourceClass, Class<T> toClass) {
        return findOneOptional(searchBuilder, sourceClass, toClass).orElse(null);
    }

    public <T> Optional<T> findOneOptional(SearchQuery searchQuery, Class<T> toClass) {
        return findOneOptional(SearchBuilder.query(searchQuery), toClass, toClass);
    }

    public <T> Optional<T> findOneOptional(SearchBuilder searchBuilder, Class<T> toClass) {
        return findOneOptional(searchBuilder, toClass, toClass);
    }

    public <T> Optional<T> findOneOptional(SearchQuery searchQuery, Class<?> sourceClass, Class<T> toClass) {
        return findOneOptional(SearchBuilder.query(searchQuery), Pageable.unpaged(), sourceClass, toClass);
    }

    public <T> Optional<T> findOneOptional(SearchBuilder searchBuilder, Class<?> sourceClass, Class<T> toClass) {
        return findOneOptional(searchBuilder, Pageable.unpaged(), sourceClass, toClass);
    }

    public <T> T firstResult(SearchQuery searchQuery, Class<?> sourceClass, Class<T> toClass) {
        return firstResultOptional(SearchBuilder.query(searchQuery), sourceClass, toClass).orElse(null);
    }

    public <T> T firstResult(SearchBuilder searchBuilder, Class<?> sourceClass, Class<T> toClass) {
        return firstResultOptional(searchBuilder, sourceClass, toClass).orElse(null);
    }

    public <T> Optional<T> firstResultOptional(SearchQuery searchQuery, Class<?> sourceClass, Class<T> toClass) {
        return findOneOptional(SearchBuilder.query(searchQuery), PageRequest.of(0, 1), sourceClass, toClass);
    }

    public <T> Optional<T> firstResultOptional(SearchBuilder searchBuilder, Class<?> sourceClass, Class<T> toClass) {
        return findOneOptional(searchBuilder, PageRequest.of(0, 1), sourceClass, toClass);
    }

    public <T> Optional<T> findOneOptional(SearchBuilder searchBuilder, Pageable pageable, Class<?> sourceClass, Class<T> toClass) {
        SearchBuilder sb = SearchBuilder.query(searchBuilder.getSearchQuery());
        Pageable pg = PageRequest.of(0, 1, pageable.getSort());

        try {
            if (autoMap(toClass, sb)) {
                List<T> results = find(sb, pg, sourceClass, toClass).getResultList();
                return CollectionUtils.isEmpty(results) ?
                        Optional.empty() :
                        Optional.ofNullable(results.get(0));
            } else {
                Object id = null;
                String idAttributeName = EntityUtil.getId(sourceClass);
                List<Tuple> tuples = find(sb, pg, sourceClass, Tuple.class).getResultList();
                if (!CollectionUtils.isEmpty(tuples)) {
                    id = tuples.get(0).get(idAttributeName);
                }

                return id == null ?
                        Optional.empty() :
                        Optional.ofNullable(new TupleToEntityMapper().mapValue(tuples, toClass).get(0));
            }
        } catch (NoResultException | ArrayIndexOutOfBoundsException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }

    }

    public <T> List<T> list(SearchQuery searchQuery, Class<T> toClass) {
        return list(SearchBuilder.query(searchQuery), toClass, toClass);
    }

    public <T> List<T> list(SearchBuilder searchBuilder, Class<T> toClass) {
        return list(searchBuilder, toClass, toClass);
    }

    public <T> List<T> list(SearchQuery searchQuery, Class<?> sourceClass, Class<T> toClass) {
        return list(SearchBuilder.query(searchQuery), Pageable.unpaged(), sourceClass, toClass);
    }

    public <T> List<T> list(SearchBuilder searchBuilder, Class<?> sourceClass, Class<T> toClass) {
        return list(searchBuilder, Pageable.unpaged(), sourceClass, toClass);
    }

    public <T> List<T> list(SearchQuery searchQuery, Pageable pageable, Class<T> toClass) {
        return list(SearchBuilder.query(searchQuery), pageable, toClass, toClass);
    }

    public <T> List<T> list(SearchBuilder searchBuilder, Pageable pageable, Class<T> toClass) {
        return list(searchBuilder, pageable, toClass, toClass);
    }

    public <T> List<T> list(SearchQuery searchQuery, Pageable pageable, Class<?> sourceClass, Class<T> toClass) {
        return list(SearchBuilder.query(searchQuery), pageable, sourceClass, toClass);
    }

    public <T> List<T> list(SearchBuilder searchBuilder, Pageable pageable, Class<?> sourceClass, Class<T> toClass) {
        try {
            SearchBuilder sb = SearchBuilder.query(searchBuilder.getSearchQuery());
            if (autoMap(toClass, sb)) {
                Pageable pg = pageable.isUnpaged() ?
                        PageRequest.of(0, Integer.MAX_VALUE, pageable.getSort()) :
                        PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), pageable.getSort());

                return find(sb, pg, sourceClass, toClass).getResultList();
            } else {
                Pageable pg = Pageable.unpaged(pageable.getSort());

                // Mengambil koleksi ID yang akan digunakan untuk pencarian paginated
                List<Object> ids = getIds(pageable, sourceClass, sb, pg);
                if (ids.isEmpty()) {
                    return Collections.emptyList();
                }

                // Menghapus semua grup lalu menggantinya dengan hasil ID di WHERE clause
                if (sb.getSearchQuery().getSearchGroup() != null) {
                    sb.getSearchQuery().setSearchGroup(null);
                }

                // Tunggu semua future selesai dan gabungkan hasilnya
                List<Tuple> tuples = fetchData(sb, sourceClass, pg, ids);

                // Mapping hasil tuples ke entitas toClass
                return new TupleToEntityMapper().mapValue(tuples, toClass);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    private List<Object> getIds(Pageable pageable, Class<?> sourceClass, SearchBuilder sb, Pageable pg) {
        String idAttribute = EntityUtil.getId(sourceClass);
        Field idField = EntityUtil.getField(sourceClass, idAttribute);
        List<Object> ids = new ArrayList<>();

        if (idField != null) {
            SearchBuilder sb2 = SearchBuilder.query(SearchQuery.copy(sb.getSearchQuery()));
            sb2.setSourceClass(sourceClass);
            Sort sort = sb2.getSearchQuery().getSort() != null ? sb2.getSearchQuery().getSort() : pageable.getSort();
            SearchQuery query = sb2.getSearchQuery();

            // select only the necessary fields, as few as possible.
            query.getSearchSelects().clear();
            query.add(SearchSelect.add(idAttribute));

            if (query.getSearchGroup() == null) {
                query.groupBy(idAttribute);

                if (!sort.isEmpty()) {
                    query.groupBy(sort.stream().map(Sort.Order::getProperty).toList());
                }
            }

            // fetch IDs
            ids = find(sb2, pg, sourceClass, Object.class).getResultList();

            if (!ids.isEmpty()) {
                long offset = !pageable.isUnpaged() ? pageable.getOffset() : 0;
                ids = ids.stream().distinct().toList();

                if (offset >= ids.size()) return Collections.emptyList();

                // process paging
                ids = ids.subList((int) offset, ids.size());

                if (!pageable.isUnpaged()) {
                    ids = ids.stream().limit(pageable.getPageSize()).toList();
                }
            }
        }

        return ids;
    }

    public List<Tuple> fetchData(SearchBuilder sb, Class<?> sourceClass, Pageable pg, List<Object> ids) {
        int maxParams = 40000;
        String idAttribute = EntityUtil.getId(sourceClass);
        List<CompletableFuture<List<Tuple>>> futures = new ArrayList<>();

        // Membagi ID menjadi batch dan menjalankan pencarian secara paralel
        for (int i = 0; i < ids.size(); i += maxParams) {
            int end = Math.min(i + maxParams, ids.size());
            List<Object> batchIds = ids.subList(i, end);

            futures.add(CompletableFuture.supplyAsync(() -> {
                // Membuat salinan SearchBuilder untuk batch ini agar kriteria pencarian tidak dihapus
                SearchBuilder localSb = SearchBuilder.query(SearchQuery.copy(sb.getSearchQuery()));

                if (localSb.getSearchQuery().getSearchCriterias() != null) {
                    localSb.getSearchQuery().getSearchCriterias().clear();
                }

                // Menambahkan kriteria pencarian IN dengan batch ID
                localSb.getSearchQuery().add(SearchCriteria.with(idAttribute, ComparissonOperator.IN, batchIds));

                // Menjalankan pencarian dan mengembalikan hasilnya
                return find(localSb, pg, sourceClass, Tuple.class).getResultList();
            }));
        }

        // Tunggu semua future selesai dan gabungkan hasilnya
        return futures.stream()
                .map(CompletableFuture::join) // Tunggu setiap future selesai
                .flatMap(List::stream)        // Gabungkan hasil dari tiap future
                .collect(Collectors.toList()); // Simpan hasil ke dalam list
    }

    public <T> Page<T> page(SearchQuery searchQuery, Class<T> toClass) {
        return page(SearchBuilder.query(searchQuery), toClass, toClass);
    }

    public <T> Page<T> page(SearchBuilder searchBuilder, Class<T> toClass) {
        return page(searchBuilder, toClass, toClass);
    }

    public <T> Page<T> page(SearchQuery searchQuery, Class<?> sourceClass, Class<T> toClass) {
        return page(SearchBuilder.query(searchQuery), Pageable.unpaged(), sourceClass, toClass);
    }

    public <T> Page<T> page(SearchBuilder searchBuilder, Class<?> sourceClass, Class<T> toClass) {
        return page(searchBuilder, Pageable.unpaged(), sourceClass, toClass);
    }

    public <T> Page<T> page(SearchQuery searchQuery, Pageable pageable, Class<T> toClass) {
        return page(SearchBuilder.query(searchQuery), pageable, toClass, toClass);
    }

    public <T> Page<T> page(SearchBuilder searchBuilder, Pageable pageable, Class<T> toClass) {
        return page(searchBuilder, pageable, toClass, toClass);
    }

    public <T> Page<T> page(SearchQuery searchQuery, Pageable pageable, Class<?> sourceClass, Class<T> toClass) {
        return page(SearchBuilder.query(searchQuery), pageable, sourceClass, toClass);
    }

    public <T> Page<T> page(SearchBuilder searchBuilder, Pageable pageable, Class<?> sourceClass, Class<T> toClass) {
        long total = count(SearchBuilder.query(SearchQuery.copy(searchBuilder.getSearchQuery())), sourceClass);
        long offset = pageable.isPaged() ? pageable.getOffset() : 0;
        List<T> list = offset >= total ?
                Collections.emptyList() :
                list(SearchBuilder.query(SearchQuery.copy(searchBuilder.getSearchQuery())), pageable, sourceClass, toClass);

        return new PageImpl<>(list, pageable, total);
    }

    public Long count(SearchBuilder searchBuilder) {
        return count(searchBuilder, searchBuilder.getSourceClass());
    }

    public Long count(SearchQuery searchQuery, Class<?> sourceClass) {
        return count(SearchBuilder.query(searchQuery), sourceClass);
    }

    public boolean exists(SearchQuery searchQuery, Class<?> sourceClass) {
        return count(SearchBuilder.query(searchQuery), sourceClass) > 0;
    }

    public Long count(SearchBuilder searchBuilder, Class<?> sourceClass) {
        if (!CollectionUtils.isEmpty(searchBuilder.getSearchQuery().getGroupBys())) {
            log.error("count with group is not supported!");
            throw new RuntimeException("count with group is not supported!");
        }
        SearchBuilder sb = SearchBuilder.query(SearchQuery.copy(searchBuilder.getSearchQuery()));
        sb.setSourceClass(sourceClass);
        sb.setCount(true);
        sb.build();

        Map<String, Object> parameters = sb.getParameters();
        TypedQuery<Long> query = entityManager.createQuery(sb.getQuery(), Long.class);
        for (String key : parameters.keySet()) {
            query.setParameter(key, parameters.get(key));
        }

        return query.getSingleResult();
    }

    private boolean autoMap(Class<?> toClass, SearchBuilder sb) {
        return toClass.getCanonicalName().equalsIgnoreCase("jakarta.persistence.Tuple") ||
                toClass.getCanonicalName().contains("java.util") ||
                toClass.getCanonicalName().contains("java.lang") ||
                CollectionUtils.isEmpty(sb.getSearchQuery().getSearchSelects()) &&
                        CollectionUtils.isEmpty(sb.getSearchQuery().getSortCases());
    }
}
