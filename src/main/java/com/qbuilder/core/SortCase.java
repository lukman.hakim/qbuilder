package com.qbuilder.core;

import lombok.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SortCase {

    private String attribute;
    private List<?> values;
    private ComparissonOperator comparissonOperator;

    public static SortCase with(String attribute, ComparissonOperator comparissonOperator, Object value) {
        List<?> values = value == null ? null : value instanceof List<?> ? (List<?>) value : Collections.singletonList(value);

        return SortCase.builder()
                .attribute(attribute)
                .values(values)
                .comparissonOperator(comparissonOperator)
                .build();
    }

    public static SortCase with(String attribute, ComparissonOperator comparissonOperator, Object... value) {
        return with(attribute, comparissonOperator, Arrays.stream(value).toList());
    }
}