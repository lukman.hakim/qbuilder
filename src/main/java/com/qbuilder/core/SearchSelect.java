package com.qbuilder.core;

import lombok.*;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SearchSelect {

    private String attribute;
    private SelectOperation selectOperation;
    private String alias;

    @Builder.Default
    private boolean isEmbeddedAttribute = false;

    /*
     * it's recommended to always use alias, especially for related attributes
     * if the alias is null then filled with attribute
     * if attribute has a relation so the dot (.) characters will be replaced with underscore (_)
     * */
    public static SearchSelect add(String attribute) {

        return add(attribute, null, normalizeAlias(attribute), false);
    }

    public static SearchSelect add(String attribute, String alias) {
        return add(attribute, null, normalizeAlias(alias), false);
    }

    public static SearchSelect add(String attribute, SelectOperation selectOperation) {
        return add(attribute, selectOperation, null, false);
    }

    public static SearchSelect withEmbedded(String attribute, SelectOperation selectOperation) {
        return add(attribute, selectOperation, null, true);
    }

    public static SearchSelect add(String attribute, SelectOperation selectOperation, String alias) {
        return add(attribute, selectOperation, alias, false);
    }

    public static SearchSelect withEmbedded(String attribute, SelectOperation selectOperation, String alias) {
        return add(attribute, selectOperation, alias, true);
    }

    public static SearchSelect add(String attribute, SelectOperation selectOperation, String alias, boolean isEmbeddedAttribute) {
        return SearchSelect.builder()
                .attribute(attribute)
                .selectOperation(selectOperation)
                .alias(alias)
                .isEmbeddedAttribute(isEmbeddedAttribute)
                .build();
    }

    public static SearchSelect min(String attribute) {
        return min(attribute, toAlias(attribute), false);
    }

    public static SearchSelect minEmbedded(String attribute) {
        return min(attribute, toAlias(attribute), true);
    }

    public static SearchSelect min(String attribute, String alias) {
        return min(attribute, alias, false);
    }

    public static SearchSelect minEmbedded(String attribute, String alias) {
        return min(attribute, alias, true);
    }

    public static SearchSelect min(String attribute, String as, boolean isEmbeddedAttribute) {
        return SearchSelect.add(attribute, SelectOperation.MIN, as, isEmbeddedAttribute);
    }

    public static SearchSelect max(String attribute) {
        return max(attribute, toAlias(attribute), false);
    }

    public static SearchSelect maxEmbedded(String attribute) {
        return max(attribute, toAlias(attribute), true);
    }

    public static SearchSelect max(String attribute, String alias) {
        return max(attribute, alias, false);
    }

    public static SearchSelect maxEmbedded(String attribute, String alias) {
        return max(attribute, alias, true);
    }

    public static SearchSelect max(String attribute, String as, boolean isEmbeddedAttribute) {
        return SearchSelect.add(attribute, SelectOperation.MAX, as, isEmbeddedAttribute);
    }

    public static SearchSelect avg(String attribute) {
        return avg(attribute, toAlias(attribute), false);
    }

    public static SearchSelect avgEmbedded(String attribute) {
        return avg(attribute, toAlias(attribute), true);
    }

    public static SearchSelect avg(String attribute, String alias) {
        return avg(attribute, alias, false);
    }

    public static SearchSelect avgEmbedded(String attribute, String alias) {
        return avg(attribute, alias, true);
    }

    public static SearchSelect avg(String attribute, String as, boolean isEmbeddedAttribute) {
        return SearchSelect.add(attribute, SelectOperation.AVG, as, isEmbeddedAttribute);
    }

    public static SearchSelect sum(String attribute) {
        return sum(attribute, toAlias(attribute), false);
    }

    public static SearchSelect sumEmbedded(String attribute) {
        return avg(attribute, toAlias(attribute), true);
    }

    public static SearchSelect sum(String attribute, String alias) {
        return sum(attribute, alias, false);
    }

    public static SearchSelect sumEmbedded(String attribute, String alias) {
        return avg(attribute, alias, true);
    }

    public static SearchSelect sum(String attribute, String as, boolean isEmbeddedAttribute) {
        return SearchSelect.add(attribute, SelectOperation.SUM, as, isEmbeddedAttribute);
    }

    public static SearchSelect distinct() {
        return distinct(null, null, false);
    }

    public static SearchSelect distinct(String attribute) {
        return distinct(attribute, toAlias(attribute), false);
    }

    public static SearchSelect distinctEmbedded(String attribute) {
        return distinct(attribute, toAlias(attribute), true);
    }

    public static SearchSelect distinct(String attribute, String alias) {
        return distinct(attribute, alias, false);
    }

    public static SearchSelect distinctEmbedded(String attribute, String alias) {
        return distinct(attribute, alias, true);
    }

    public static SearchSelect distinct(String attribute, String as, boolean isEmbeddedAttribute) {
        return SearchSelect.add(attribute, SelectOperation.DISTINCT, as, isEmbeddedAttribute);
    }

    private static String toAlias(String attribute) {
        String[] parts = attribute.split("\\.");
        StringBuilder alias = new StringBuilder(parts[0]);
        for (int i = 1; i < parts.length; i++) {
            alias.append(Character.toUpperCase(parts[i].charAt(0))).append(parts[i].substring(1));
        }

        return alias.toString();
    }

    private static String normalizeAlias(String alias) {
        return alias.replaceAll("\\.", "_");
    }
}