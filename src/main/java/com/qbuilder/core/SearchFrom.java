package com.qbuilder.core;

import jakarta.persistence.criteria.JoinType;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class SearchFrom {

    private String from;
    private JoinType joinType;

    public static SearchFrom add(String attributePath) {
        return add(attributePath, JoinType.INNER);
    }

    public static SearchFrom add(String from, JoinType joinType) {
        return SearchFrom.builder()
                .from(from)
                .joinType(joinType)
                .build();
    }

    public static SearchFrom add(String from, String joinType) {
        return SearchFrom.builder()
                .from(from)
                .joinType(joinType(joinType))
                .build();
    }

    public static JoinType joinType(String joinType) {
        return switch (joinType) {
            case "LEFT", "LEFT JOIN" -> JoinType.LEFT;
            case "RIGHT", "RIGHT JOIN" -> JoinType.RIGHT;
            case "INNER", "JOIN", "INNER JOIN" -> JoinType.INNER;
            default -> throw new RuntimeException("not supported join type: " + joinType);
        };
    }

    public static String joinType(JoinType joinType) {
        return switch (joinType) {
            case LEFT -> "LEFT JOIN";
            case RIGHT -> "RIGHT JOIN";
            case INNER -> "JOIN";
        };
    }
}