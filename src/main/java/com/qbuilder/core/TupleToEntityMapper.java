package com.qbuilder.core;

import jakarta.persistence.Tuple;
import jakarta.persistence.TupleElement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.concurrent.*;

@Slf4j
public class TupleToEntityMapper {

    private static final Map<Class<?>, Constructor<?>> constructorCache = new ConcurrentHashMap<>();
    private static final Map<Class<?>, String> idAttrCache = new ConcurrentHashMap<>();
    private static final Map<String, Field> fieldCache = new ConcurrentHashMap<>();
    private static final Map<String, Object> fieldValueCache = new ConcurrentHashMap<>();

    public <T> List<T> mapValue(List<Tuple> tuples, Class<T> toClass) {
        try {
            // group value by root id
            Map<Object, List<Tuple>> groupTuples = groupTuple(tuples, toClass);
            // process map value
            Map<Object, T> groupValue = groupValue(toClass, groupTuples);
            return groupValue.values().stream().toList();
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException |
                 ArrayIndexOutOfBoundsException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    private <T> Map<Object, List<Tuple>> groupTuple(List<Tuple> tuples, Class<T> toClass) throws NoSuchMethodException {
        Map<Object, List<Tuple>> groupTuples = new LinkedHashMap<>();
        for (Tuple tuple : tuples) {
            String rootIdAttr = getIdAttr(toClass);
            Object rootId = tuple.get(rootIdAttr);

            if (!groupTuples.containsKey(rootId)) {
                groupTuples.put(rootId, new ArrayList<>());
            }

            List<Tuple> ts = groupTuples.get(rootId);
            ts.add(tuple);
        }

        return groupTuples;
    }

    private <T> Map<Object, T> groupValue(Class<T> toClass, Map<Object, List<Tuple>> groupTuples) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Map<Object, T> result = new LinkedHashMap<>();
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        for (Object key : groupTuples.keySet()) {
            ConcurrentHashMap<Object, Object> mapParents = new ConcurrentHashMap<>();
            List<Tuple> tuples = groupTuples.get(key);
            List<CompletableFuture<Void>> futureTupleProcesses = new ArrayList<>();

            for (Tuple tuple : tuples) {
                Object rootId = initateRoot(tuple, toClass, mapParents);
                futureTupleProcesses.add(CompletableFuture.runAsync(() -> setValue(tuple, rootId, mapParents), executorService));
            }

            CompletableFuture.allOf(futureTupleProcesses.toArray(new CompletableFuture[0])).join();
            result.putIfAbsent(key, (T) mapParents.get(key));
        }

        executorService.shutdown();
        return result;
    }

    private <T> Object initateRoot(Tuple tuple, Class<T> toClass, ConcurrentHashMap<Object, Object> mapParents) throws InvocationTargetException, InstantiationException, IllegalAccessException {
        // Get the cached constructor or compute it if not already present
        Constructor<T> constructor = getConstructor(toClass);
        String rootIdAttr = getIdAttr(toClass);
        Object rootId = tuple.get(rootIdAttr);

        mapParents.putIfAbsent(rootId, constructor.newInstance());

        return rootId;
    }

    public <T> void setValue(Tuple tuple, Object rootId, ConcurrentHashMap<Object, Object> mapParents) {
        if (CollectionUtils.isEmpty(tuple.getElements()) || tuple.getElements().get(0).getAlias() == null) return;
        try {
            ConcurrentHashMap<String, Object> mapChildrens = new ConcurrentHashMap<>();
            T result = (T) mapParents.get(rootId);
            for (TupleElement<?> element : tuple.getElements()) {
                try {
                    String key = element.getAlias();

                    if (key.equalsIgnoreCase(SearchBuilder.SORT_CASE_ALIAS)) continue;

                    setChildrenValue(key, key, result, tuple, mapParents, mapChildrens);
                    String keyMasked = key.replace("_", ".");
                    Field field = getField(result.getClass(), keyMasked);

                    if (field == null) continue;

                    field.set(result, tuple.get(keyMasked));

                } catch (InvocationTargetException | IllegalAccessException | NoSuchFieldException |
                         ClassNotFoundException | ArrayIndexOutOfBoundsException e) {
                    log.error(e.getMessage(), e);
                    throw new RuntimeException(e);
                }
            }

        } catch (NoSuchMethodException | InstantiationException | ArrayIndexOutOfBoundsException ex) {
            log.error(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }

    private void setChildrenValue(String alias, String attribute, Object parent, Tuple tuple, ConcurrentHashMap<Object, Object> mapParents, ConcurrentHashMap<String, Object> mapChildren) throws NoSuchFieldException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        try {
            if (!attribute.contains("_")) return;
            List<String> attributeSplit = Arrays.asList(attribute.split("_"));
            for (int i = 0; i < attributeSplit.size() - 1; i++) {
                if (i + 1 != attributeSplit.size() - 1) continue;
                Object tupleValue = tuple.get(alias);

                if (tupleValue == null) {
                    continue;
                }
                String parentAttr = attributeSplit.get(i);
                String childAttr = attributeSplit.get(i + 1);
                String key = String.join(".", attributeSplit.subList(0, i + 1));
                List<String> keySplit = Arrays.asList(key.split("\\."));
                parent = keySplit.size() > 1 ? mapChildren.get(String.join(".", keySplit.subList(0, i))) : parent;

                if (parent == null) continue;

                Field parentField = getField(parent.getClass(), parentAttr);

                if (parentField == null) continue;

                String canonicalName = parentField.getType().getCanonicalName();
                String parentIdAttr = getIdAttr(parent.getClass());
                Field parentIdField = getField(parent.getClass(), parentIdAttr);

                if (parentIdField == null) continue;

                Object parentIdValue = getFieldValue(parentIdField, parent);
                Constructor<?> parentConstructor = getConstructor(Class.forName(parent.getClass().getCanonicalName()));
                if (parentIdValue != null) {
                    mapParents.compute(parentIdValue, (k, v) -> {
                        try {
                            return Objects.requireNonNullElse(v, parentConstructor.newInstance());
                        } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
                                 ArrayIndexOutOfBoundsException e) {
                            log.error(e.getMessage(), e);
                            throw new RuntimeException(e);
                        }
                    });

                    parent = mapParents.get(parentIdValue);
                }

                if (canonicalName.startsWith("jakarta") || canonicalName.startsWith("javax") || canonicalName.startsWith("jdk"))
                    continue;

                if (canonicalName.startsWith("java")) {
                    processCollectionChild(parent, mapParents, mapChildren, parentField, canonicalName, key, childAttr, tupleValue, parentAttr);

                } else {
                    processChild(parent, mapParents, mapChildren, canonicalName, key, childAttr, tupleValue, parentField);
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    private static void processChild(Object parent, ConcurrentHashMap<Object, Object> mapParents, ConcurrentHashMap<String, Object> mapChildren, String canonicalName, String key, String childAttr, Object tupleValue, Field parentField) throws NoSuchMethodException, ClassNotFoundException, InstantiationException, IllegalAccessException, InvocationTargetException {
        try {
            Constructor<?> childConstructor = getConstructor(Class.forName(canonicalName));
            mapChildren.putIfAbsent(key, childConstructor.newInstance());
            Object child = mapChildren.get(key);


            if (child == null) return;

            Field childField = getField(child.getClass(), childAttr);
            String childIdAttr = getIdAttr(child.getClass());
            Field childIdField = getField(child.getClass(), childIdAttr);
            Object childIdValue = getFieldValue(childIdField, child);
            childIdValue = childIdValue != null ? childIdValue : tupleValue;
            child = mapParents.computeIfAbsent(childIdValue, k -> mapChildren.get(key));
            mapChildren.put(key, child);

            if (childField == null) return;
            childField.set(child, tupleValue);
            parentField.set(parent, child);
        } catch (IllegalArgumentException | ArrayIndexOutOfBoundsException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    private static void processCollectionChild(Object parent, ConcurrentHashMap<Object, Object> mapParents, ConcurrentHashMap<String, Object> mapChildren, Field parentField, String canonicalName, String key, String childAttr, Object tupleValue, String parentAttr) throws IllegalAccessException, NoSuchMethodException, InstantiationException, InvocationTargetException {
        try {
            ParameterizedType pType = (ParameterizedType) parentField.getGenericType();
            Class<?> genericChildClass = (Class<?>) pType.getActualTypeArguments()[0];

            if (canonicalName.equalsIgnoreCase(List.class.getCanonicalName()) || canonicalName.equalsIgnoreCase(Set.class.getCanonicalName())) {
                boolean isList = canonicalName.equalsIgnoreCase(List.class.getCanonicalName());
                var childCollection = (Collection<Object>) parentField.get(parent);

                if (childCollection == null) {
                    childCollection = isList ? new CopyOnWriteArrayList<>() : Collections.newSetFromMap(new ConcurrentHashMap<>());
                    parentField.set(parent, childCollection);
                }

                Constructor<?> childConstructor = getConstructor(genericChildClass);
                mapChildren.putIfAbsent(key, childConstructor.newInstance());
                Object child = mapChildren.get(key);
                if (child == null) return;

                Field childField = getField(child.getClass(), childAttr);
                String childIdAttr = getIdAttr(child.getClass());
                Field childIdField = getField(child.getClass(), childIdAttr);
                Object childIdValue = getFieldValue(childIdField, child);
                childIdValue = childIdValue != null ? childIdValue : tupleValue;

                child = mapParents.computeIfAbsent(childIdValue, k -> mapChildren.get(key));
                mapChildren.put(key, child);

                if (childField == null) return;
                childField.set(child, tupleValue);

                Object finalChildIdValue = childIdValue;
                boolean add = new ArrayList<>(childCollection).parallelStream()
                        .filter(c -> !ObjectUtils.isEmpty(c))
                        .noneMatch(c -> {
                            String cIdAttr = getIdAttr(c.getClass());
                            Field cFiled = getField(c.getClass(), cIdAttr);
                            Object cIdValue = getFieldValue(cFiled, c);
                            return Objects.equals(cIdValue, finalChildIdValue);
                        });

                // Menambahkan child jika tidak ada duplikat ID
                if (childCollection.isEmpty() || add) {
                    childCollection.add(child);
                }

            } else {
                throw new RuntimeException(parent + " attribute " + parentAttr + " type " + canonicalName + " not supported!");
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    private static <T> Constructor<T> getConstructor(Class<T> clazz) {
        return (Constructor<T>) constructorCache.computeIfAbsent(clazz, cls -> {
            try {
                Constructor<T> constructor = (Constructor<T>) cls.getDeclaredConstructor();
                constructor.setAccessible(true);
                return constructor;
            } catch (NoSuchMethodException e) {
                throw new RuntimeException("Constructor not found for class: " + cls, e);
            }
        });
    }

    private static <T> String getIdAttr(Class<T> clazz) {
        return idAttrCache.computeIfAbsent(clazz, EntityUtil::getId);
    }

    public static Field getField(Class<?> clazz, String fieldName) {
        // Generate a unique key for the cache, combining class name and field name
        String cacheKey = clazz.getCanonicalName() + "#" + fieldName;

        // Get the cached field or compute it if not already present
        Field field = fieldCache.computeIfAbsent(cacheKey, key -> {
            try {
                // Fetch the field using EntityUtil (or reflection)
                Field f = EntityUtil.getField(clazz, fieldName);

                if (f != null) {
                    f.setAccessible(true);
                }

                return f;
            } catch (Exception e) {
                throw new RuntimeException("Failed to get field: " + fieldName + " from class: " + clazz.getName(), e);
            }
        });

        return field;
    }


    public static Object getFieldValue(Field field, Object obj) {
        // Get the cached parentIdValue or compute it if not already present
        return fieldValueCache.computeIfAbsent(obj.toString(), key -> {
            try {
                // Fetch the parentIdValue using EntityUtil.getFieldValue
                return EntityUtil.getFieldValue(field, obj);
            } catch (Exception e) {
                throw new RuntimeException("Failed to get parent ID value for class: " + obj.getClass().getName());
            }
        });
    }
}
