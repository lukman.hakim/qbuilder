package com.qbuilder.core;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class RsqlHelper {

    private static final String PARAM_NAME = "rsqlParam";
    private static final String MAP_VALUE_SEPARATOR = "||";

    public static final Map<String, String> RSQL_OPERATOR_MAP = Map.ofEntries(
            Map.entry("==", "="),
            Map.entry("!=", "!="),
            Map.entry("=gt=", ">"),
            Map.entry("=lt=", "<"),
            Map.entry("=ge=", ">="),
            Map.entry("=le=", "<="),
            Map.entry("=in=", "IN"),
            Map.entry("=out=", "NOT IN"),
            Map.entry("=like=", "LIKE"),
            Map.entry("=ilike=", "LIKE"),
            Map.entry("=null=", "IS NULL"),
            Map.entry("=isnull=", "IS NULL"),
            Map.entry("=notnull=", "IS NOT NULL"),
            Map.entry("=isnotnull=", "IS NOT NULL"),
            Map.entry("=between=", "BETWEEN"),
            Map.entry("=bt=", "BETWEEN"),
            Map.entry("=ik=", "LIKE"),
            Map.entry("=nb=", "NOT BETWEEN"),
            Map.entry("=na=", "IS NULL"),
            Map.entry("=nn=", "IS NOT NULL")
    );

    public static List<String> getAttributes(SearchRsql searchRsql) {
        List<String> attributes = new ArrayList<>();

        for (String operator : searchRsql.getOperators()) {
            String regex = RsqlHelper.getRegex(operator);

            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(searchRsql.getRsql());
            while (matcher.find()) {
                attributes.add(matcher.group(1));
            }
        }

        return attributes;
    }

    public static String toJpql(SearchBuilder sb) {
        SearchRsql searchRsql = sb.getSearchQuery().getSearchRsql();
        Map<String, String> keyValue = new HashMap<>();

        if (searchRsql == null || StringUtils.isBlank(searchRsql.getRsql()))
            throw new IllegalArgumentException("SearchRSQL is null");

        String jpql = searchRsql.getRsql();

        // proses operator yang ditemukan aja.
        Counter counter = new Counter();

        for (String operator : searchRsql.getOperators()) {
            jpql = processOperator(sb, keyValue, jpql, operator, counter);
        }

        // Regex untuk menemukan , di luar tanda '
        Pattern commaPattern = Pattern.compile(",(?=(?:[^']*'[^']*')*[^']*$)");
        Matcher commaMatcher = commaPattern.matcher(jpql);

        // Mengganti , di luar tanda ' dengan "OR"
        jpql = commaMatcher.replaceAll(" OR ");

        // Regex untuk menemukan ; di luar tanda '
        Pattern semicolonPattern = Pattern.compile(";(?=(?:[^']*'[^']*')*[^']*$)");
        Matcher semicolonMatcher = semicolonPattern.matcher(jpql);

        // Mengganti ; di luar tanda ' dengan "AND"
        jpql = semicolonMatcher.replaceAll(" AND ");
        jpql = jpql.replace("|-|", ",");

        return jpql;
    }

    private static String processOperator(SearchBuilder sb, Map<String, String> keyValue, String input, String operator, Counter counter) {
        String regex = getRegex(operator);
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);

        while (matcher.find()) {
            String path = matcher.group(1);
            Class<?> clazz = EntityUtil.getClassByAttributePath(path, sb.getSourceClass());
            String valueStr1;
            String valueStr2;
            Object value1;
            Object value2;
            String sqlOperator = RSQL_OPERATOR_MAP.get(operator);

            switch (operator) {
                case "==":
                case "!=":
                case "=gt=":
                case "=lt=":
                case "=ge=":
                case "=le=": {
                    valueStr1 = matcher.group(3);
                    String mapValue = path + MAP_VALUE_SEPARATOR + valueStr1;

                    if (keyValue.values().stream().anyMatch(v -> v.equalsIgnoreCase(mapValue))) continue;

                    regex = getRegex(operator, path, valueStr1);
                    value1 = convertValue(clazz, valueStr1);
                    input = input.replaceAll(regex, "$1 " + sqlOperator + " :" + PARAM_NAME + counter.get());

                    keyValue.put(PARAM_NAME + counter.get(), mapValue);
                    sb.getParameters().put(PARAM_NAME + counter.getAndIncrement(), value1);
                    break;
                }
                case "=in=":
                case "=out=": {
                    valueStr1 = matcher.group(3);
                    String mapValue = path + MAP_VALUE_SEPARATOR + valueStr1;

                    if (keyValue.values().stream().anyMatch(v -> v.equalsIgnoreCase(mapValue))) continue;

                    regex = getRegex(operator, path, valueStr1);
                    value1 = convertValue(List.class, valueStr1, clazz);  // Convert the valueStr1 to a List
                    input = input.replaceAll(regex, "$1 " + sqlOperator + " (:" + PARAM_NAME + counter.get() + ")");

                    keyValue.put(PARAM_NAME + counter.get(), mapValue);
                    sb.getParameters().put(PARAM_NAME + counter.getAndIncrement(), value1);
                    break;
                }
                case "=like=":
                case "=ik=": {
                    valueStr1 = matcher.group(3);
                    String mapValue = path + MAP_VALUE_SEPARATOR + valueStr1;

                    if (keyValue.values().stream().anyMatch(v -> v.equalsIgnoreCase(mapValue))) continue;

                    regex = getRegex(operator, path, valueStr1);
                    value1 = convertValue(clazz, valueStr1);
                    input = input.replaceAll(regex, "$1 LIKE CONCAT('%'|-|:" + PARAM_NAME + counter.get() + "|-| '%')");

                    keyValue.put(PARAM_NAME + counter.get(), mapValue);
                    sb.getParameters().put(PARAM_NAME + counter.getAndIncrement(), value1);
                    break;
                }
                case "=ilike=": {
                    valueStr1 = matcher.group(3);
                    String mapValue = path + MAP_VALUE_SEPARATOR + valueStr1;

                    if (keyValue.values().stream().anyMatch(v -> v.equalsIgnoreCase(mapValue))) continue;

                    regex = getRegex(operator, path, valueStr1);
                    value1 = convertValue(clazz, valueStr1);
                    input = input.replaceAll(regex, "LOWER($1) LIKE CONCAT('%'|-|LOWER(:" + PARAM_NAME + counter.get() + ")|-| '%')");

                    keyValue.put(PARAM_NAME + counter.get(), mapValue);
                    sb.getParameters().put(PARAM_NAME + counter.getAndIncrement(), value1);
                    break;
                }
                case "=null=":
                case "=na=": {
                    regex = getRegex(operator, path);
                    input = input
                            .replace("=isnull=''", "=isnull=")
                            .replace("=null=''", "=null=")
                            .replace("=na=''", "=na=")
                            .replaceAll(regex, "$1 IS NULL");
                    break;
                }
                case "=isnotnull=":
                case "=notnull=":
                case "=nn=": {
                    regex = getRegex(operator, path);
                    input = input
                            .replace("=isnotnull=''", "=isnotnull=")
                            .replace("=notnull=''", "=notnull=")
                            .replace("=nn=''", "=nn=")
                            .replaceAll(regex, "$1 IS NOT NULL");
                    break;
                }
                case "=between=":
                case "=bt=":
                case "=nb=": {
                    valueStr1 = matcher.group(3);
                    valueStr2 = matcher.group(4);
                    String mapValue = path + MAP_VALUE_SEPARATOR + valueStr1 + MAP_VALUE_SEPARATOR + valueStr2;

                    if (keyValue.values().stream().anyMatch(v -> v.equalsIgnoreCase(mapValue))) continue;

                    value1 = convertValue(clazz, valueStr1);
                    value2 = convertValue(clazz, valueStr2);
                    regex = getRegex(operator, path, valueStr1, valueStr2);
                    input = input.replaceAll(regex, "$1 " + sqlOperator + " :" + PARAM_NAME + counter.get() + " AND :" + PARAM_NAME + (counter.get() + 1));

                    keyValue.put(PARAM_NAME + counter.get(), mapValue);
                    sb.getParameters().put(PARAM_NAME + counter.getAndIncrement(), value1);
                    sb.getParameters().put(PARAM_NAME + counter.getAndIncrement(), value2);
                    break;
                }
            }
        }

        return input;
    }

    private static Object convertValue(Class<?> clazz, String valueStr) {
        return convertValue(clazz, valueStr, null);
    }

    private static Object convertValue(Class<?> clazz, String valueStr, Class<?> sourceClazz) {
        boolean isCollection = clazz.equals(List.class) || clazz.equals(Set.class);

        if (isCollection && sourceClazz == null)
            throw new IllegalArgumentException("Collection type must pass sourceClazz param");

        if (valueStr.startsWith("'") && !isCollection) {
            valueStr = valueStr.substring(1, valueStr.length() - 1);
        }

        valueStr = valueStr.trim();

        if (clazz.isEnum()) {
            return Enum.valueOf((Class<Enum>) clazz, valueStr);
        } else if (clazz.equals(BigDecimal.class)) {
            return new BigDecimal(valueStr);
        } else if (clazz.equals(BigInteger.class)) {
            return new BigInteger(valueStr);
        } else if (clazz.equals(LocalDate.class)) {
            return LocalDate.parse(valueStr);
        } else if (clazz.equals(LocalDateTime.class)) {
            return LocalDateTime.parse(valueStr);
        } else if (clazz.equals(Long.class)) {
            return Long.parseLong(valueStr);
        } else if (clazz.equals(Integer.class)) {
            return Integer.parseInt(valueStr);
        } else if (clazz.equals(Float.class)) {
            return Float.parseFloat(valueStr);
        } else if (clazz.equals(Double.class)) {
            return Double.parseDouble(valueStr);
        } else if (clazz.equals(Boolean.class)) {
            return Boolean.parseBoolean(valueStr);
        } else if (clazz.equals(String.class)) {
            return valueStr;
        } else if (clazz.equals(Set.class)) {
            return Arrays.stream(valueStr.split(","))
                    .map(e -> convertValue(sourceClazz, e.trim()))
                    .collect(Collectors.toSet());
        } else if (clazz.equals(List.class)) {
            return Arrays.stream(valueStr.split(","))
                    .map(e -> convertValue(sourceClazz, e.trim()))  // Recursive call
                    .collect(Collectors.toList());
        } else {
            return valueStr;  // Default fallback
        }
    }

    public static Set<String> getOperators(String rsql) {
        Set<String> operators = new HashSet<>();

        for (String operator : RSQL_OPERATOR_MAP.keySet()) {
            String regex = getRegex(operator);
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(rsql);

            while (matcher.find()) {
                String o = matcher.group(2);
                // @todo remove if todo fixme for null operators solved
                o = o.endsWith("=") ? o : "=" + o + "=";
                operators.add(o);
            }
        }

        return operators;
    }

    public static String getRegex(String operator) {
        return getRegex(operator, null, null);
    }

    public static String getRegex(String operator, String path) {
        return getRegex(operator, path, null);
    }

    public static String getRegex(String operator, String path, String... valueStrs) {
        String attrMatcher = StringUtils.isNotBlank(path) ? path.replace(".", "\\.") : "[a-zA-Z0-9._]+";
        String valMatcher;
        boolean notNullValue = valueStrs != null;
        String valueStr = notNullValue && valueStrs.length == 1 ? valueStrs[0] : null;

        switch (operator) {
            case "=between=":
            case "=bt=":
            case "=nb=": {
                if (notNullValue && valueStrs.length != 2) throw new IllegalArgumentException("Invalid between value");

                valMatcher = notNullValue ? "(" + Pattern.quote(valueStrs[0]) + ")\\s*,\\s*(" + Pattern.quote(valueStrs[1]) + ")" : "([^,()']+|'[^']*')\\s*,\\s*([^,()']+|'[^']*')";
                return "(?<!\\.)\\b(" + attrMatcher + ")\\s*(" + operator + ")\\(" + valMatcher + "\\)";
            }
            case "=null=":
            case "=na=":
            case "=isnull=":
            case "=isnotnull=":
            case "=notnull=":
            case "=nn=": {
                // @todo fixme regex result null, na, etc, expected =null=, =na=, etc
                return "(?<!\\.)\\b(" + attrMatcher + ")\s*=(null=null|isnull|null|na|nn|notnull|isnotnull)\s*([^,;()']+|'[^']*')";
            }
            case "=in=":
            case "=out=": {
                valMatcher = StringUtils.isNotBlank(valueStr) ? Pattern.quote(valueStr) : "([^)]*)";
                return "(?<!\\.)\\b(" + attrMatcher + ")(" + operator + ")\\(" + valMatcher + "\\)";
            }
            default: {
                valMatcher = StringUtils.isNotBlank(valueStr) ? Pattern.quote(valueStr) : "[^,;()']+|'[^']*'";
//                valMatcher = StringUtils.isNotBlank(valueStr) ? valueStr : "[^,;()']+|'[^']*'";
                return "(?<!\\.)\\b(" + attrMatcher + ")\\s*(" + operator + ")\\s*(" + valMatcher + ")";
            }
        }
    }

    @Getter
    @Setter
    private static class Counter {
        private int count = 0;

        public int getAndIncrement() {
            int c = this.count;
            this.count = this.count + 1;
            return c;
        }

        public int get() {
            return this.count;
        }
    }
}

