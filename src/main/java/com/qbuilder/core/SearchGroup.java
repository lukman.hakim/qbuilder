package com.qbuilder.core;

import lombok.*;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.List;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SearchGroup {

    private List<String> attributes;
    private Sort sort;

    public static SearchGroup with(String... groupAttributes) {
        return with(null, Arrays.asList(groupAttributes));
    }

    public static SearchGroup with(List<String> groupAttributes) {
        return with(null, groupAttributes);
    }

    public static SearchGroup with(Sort sort, String... groupAttributes) {
        return with(sort, Arrays.asList(groupAttributes));
    }

    public static SearchGroup with(Sort.Direction sortDirection, String sortAttribute,  String... groupAttributes) {
        return with(Sort.by(sortDirection, sortAttribute), Arrays.asList(groupAttributes));
    }

    public static SearchGroup with(Sort sort, List<String> groupAttributes) {
        if (groupAttributes == null || groupAttributes.isEmpty()) {
            throw new RuntimeException("SearchGroup attributes can't be empty");
        }

        for (String attribute : groupAttributes) {
            if (attribute.contains("\\.")) {
                throw new RuntimeException("SearchGroup only can be group by root attribute");
            }
        }

        if (sort.stream().toList().size() > 1) {
            throw new RuntimeException("SearchGroup only can be Sort by 1 attribute");
        }

        for (Sort.Order order : sort.stream().toList()) {
            if (order.getProperty().contains("\\.")) {
                throw new RuntimeException("SearchGroup only can be Sort by root attribute");
            }
        }

        return SearchGroup.builder()
                .sort(sort)
                .attributes(groupAttributes)
                .build();
    }
}