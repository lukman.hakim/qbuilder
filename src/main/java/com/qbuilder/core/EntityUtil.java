package com.qbuilder.core;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import org.apache.commons.lang3.StringUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class EntityUtil {

    public static boolean isEntity(Class<?> entityClass) {
        Annotation[] annotations = entityClass.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof Entity || annotation instanceof Table) {
                return true;
            }
        }

        return false;
    }

    public static String getId(Class<?> entityClass) {
        for (Field field : entityClass.getDeclaredFields()) {
            if (field.isAnnotationPresent(Id.class)) {
                return field.getName();
            }
        }

        Class<?> superClass = entityClass.getSuperclass();
        if (superClass != null) {
            return getId(superClass);
        }

        return null;
    }

    public static Field getField(Class<?> clazz, String fieldName) {
        try {
            Class<?> currentClass = clazz;

            while (currentClass != null) {
                for (Field field : currentClass.getDeclaredFields()) {
                    if (field.getName().equals(fieldName)) {
                        return StringUtils.isNotBlank(fieldName) ? currentClass.getDeclaredField(fieldName) : null;
                    }
                }

                currentClass = currentClass.getSuperclass();
            }
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e.getMessage());
        }

        return null;
    }

    public static Object getFieldValue(Object obj, String fieldName) {
        Class<?> currentClass = obj.getClass();

        while (currentClass != null) {
            Field field = getField(currentClass, fieldName);
            if (field != null) {
                return getFieldValue(field, obj);
            } else {
                currentClass = currentClass.getSuperclass();
            }
        }

        throw new RuntimeException("Field tidak ditemukan: " + fieldName);
    }

    public static Object getFieldValue(Field field, Object obj) {
        try {
            field.setAccessible(true);
            return field.get(obj);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Field tidak bisa diakses: " + field.getName(), e);
        }
    }

    public static Class<?> getClassByPath(String attributePath, Class<?> entityClass) {
        String[] attributeSplit = attributePath.split("\\.");
        List<String> attributes = Arrays.asList(attributeSplit);
        List<String> attributePaths = attributes.size() > 1 ? attributes.subList(0, attributes.size() - 1) : attributes;

        Class<?> targetClass = entityClass;

        try {
            for (String attribute : attributePaths) {
                Field field = getField(targetClass, attribute);
                if (field == null) continue;
                String canonicalName = field.getType().getCanonicalName();

                if (canonicalName.startsWith("java") && (canonicalName.equalsIgnoreCase(List.class.getCanonicalName()) || canonicalName.equalsIgnoreCase(Set.class.getCanonicalName()))) {
                    ParameterizedType pType = (ParameterizedType) field.getGenericType();
                    Class<?> genericChildClass = (Class<?>) pType.getActualTypeArguments()[0];
                    targetClass = genericChildClass.getDeclaredConstructor().newInstance().getClass();
                } else {
                    targetClass = Class.forName(canonicalName).getDeclaredConstructor().newInstance().getClass();
                }
            }
        } catch (NoSuchMethodException | ClassNotFoundException | InvocationTargetException | InstantiationException |
                 IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        return targetClass;
    }

    public static Field getFieldByAttributePath(String attributePath, Class<?> entityClass) {
        String[] attributeSplit = attributePath.split("\\.");
        String attribute = Arrays.asList(attributeSplit).get(attributeSplit.length - 1);
        Class<?> targetClass = attributeSplit.length > 1 ? getClassByPath(attributePath, entityClass) : entityClass;

        return getField(targetClass, attribute);
    }

    public static Annotation[] getAnnotationsByAttributePath(String attributePath, Class<?> entityClass) {
        Field targetField = getFieldByAttributePath(attributePath, entityClass);

        if (targetField != null) {
            return targetField.getAnnotations();
        }

        return null;
    }

    public static Class<?> getClassByAttributePath(String attributePath, Class<?> entityClass) {
        Field targetField = getFieldByAttributePath(attributePath, entityClass);

        if (targetField != null) {
            return targetField.getType();
        }

        return null;
    }

    public static String getEntityName(Class<?> sourceClass) {
        return StringUtils.uncapitalize(StringUtils.join(StringUtils.splitByCharacterTypeCamelCase(sourceClass.getSimpleName()), "")) + "RootEntity";
    }

}