package com.qbuilder.core;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class EnumUtil {

    public static List<?> getContaining(Class<?> enumClass, String fieldName, String text) {
        List<Object> matchingEnums = new ArrayList<>();

        if (!enumClass.isEnum()) {
            throw new IllegalArgumentException(enumClass.getName() + " is not an enum type.");
        }

        try {
            for (Object enumConstant : enumClass.getEnumConstants()) {
                Field field = enumClass.getDeclaredField(fieldName);
                field.setAccessible(true); // Allow access to private fields
                Object value = field.get(enumConstant);

                if (value != null && value.toString().toLowerCase().contains(text.toLowerCase())) {
                    matchingEnums.add(enumConstant);
                }
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        return matchingEnums;
    }

    public static List<?> getContainingText(Class<?> enumClass, String text) {
        return getContaining(enumClass, "text", text);
    }
}