package com.qbuilder.core;

import lombok.*;
import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SearchCriteria {

    private List<SearchCriteria> searchCriterias;
    private String attribute;
    private List<?> values;
    private ComparissonOperator comparissonOperator;
    private LogicalOperator logicalOperator;

    @Builder.Default
    private boolean caseSensitive = true;

    @Builder.Default
    private boolean toChar = false;

    @Builder.Default
    private boolean isEmbeddedAttribute = false;

    public static SearchCriteria with(String attribute, ComparissonOperator operator) {
        return SearchCriteria.builder()
                .attribute(attribute)
                .comparissonOperator(operator)
                .build();
    }

    public static SearchCriteria with(String attribute, ComparissonOperator operator, Object value, boolean tochar, boolean caseSensitive, boolean embeddedAttribute) {
        List<?> values = value == null ? null : value instanceof List<?> ? (List<?>) value : Collections.singletonList(value);

        if (operator == ComparissonOperator.BETWEEN && CollectionUtils.isNotEmpty(values) && values.size() == 2 && values.get(0) == null && values.get(1) == null) {
            values = null;
        }

        return SearchCriteria.builder()
                .attribute(attribute)
                .values(values)
                .comparissonOperator(operator)
                .toChar(tochar)
                .caseSensitive(caseSensitive)
                .isEmbeddedAttribute(embeddedAttribute)
                .build();
    }

    public static SearchCriteria with(String attribute, ComparissonOperator operator, Object value) {
        return with(attribute, operator, value, false, true, false);
    }

    public static SearchCriteria withEnum(String attribute, Class<?> enumClass, String value) {
        return value != null ?
                with(attribute, ComparissonOperator.IN, BaseEnum.getContainingText(enumClass, value), false, true, false) :
                with(attribute, ComparissonOperator.IN,null, false, true, false);
    }

    public static SearchCriteria withEnumNot(String attribute, Class<?> enumClass, String value) {
        return value != null ?
                with(attribute, ComparissonOperator.NOT_IN, BaseEnum.getContainingText(enumClass, value), false, true, false) :
                with(attribute, ComparissonOperator.NOT_IN,null, false, true, false);
    }

    public static SearchCriteria with(String attribute, ComparissonOperator operator, Object... value) {
        return with(attribute, operator, Arrays.stream(value).toList(), false, true, false);
    }

    public static SearchCriteria withAsChar(String attribute, ComparissonOperator operator, Object value) {
        return with(attribute, operator, value, true, true, false);
    }

    public static SearchCriteria withEmbedded(String attribute, ComparissonOperator operator, Object value) {
        return with(attribute, operator, value, false, false, true);
    }

    public static SearchCriteria withInsensitive(String attribute, ComparissonOperator operator, Object value) {
        return with(attribute, operator, value, false, false, false);
    }

    public static SearchCriteria withInsensitiveAsChar(String attribute, ComparissonOperator operator, Object value) {
        return with(attribute, operator, value, true, false, false);
    }

    public static SearchCriteria or(SearchCriteria... searchCriterias) {
        return or(new ArrayList<>(Arrays.asList(searchCriterias)));
    }

    public static SearchCriteria or(List<SearchCriteria> searchCriterias) {
        return SearchCriteria.builder()
                .searchCriterias(searchCriterias)
                .logicalOperator(LogicalOperator.OR)
                .build();
    }

    public static SearchCriteria and(SearchCriteria... searchCriterias) {
        return and(new ArrayList<>(Arrays.asList(searchCriterias)));
    }

    public static SearchCriteria and(List<SearchCriteria> searchCriterias) {
        return SearchCriteria.builder()
                .searchCriterias(searchCriterias)
                .logicalOperator(LogicalOperator.AND)
                .build();
    }

}