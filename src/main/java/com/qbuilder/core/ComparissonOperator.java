package com.qbuilder.core;

import lombok.Getter;

@Getter
public enum ComparissonOperator {
    EQUAL("="),
    GREATER_THAN_EQUAL(">="),
    LESS_THAN_EQUAL("<="),
    GREATER_THAN(">"),
    LESS_THAN("<"),
    BETWEEN("BETWEEN"),
    NOT_BETWEEN("BETWEEN"),
    NOT_EQUAL("!="),
    NOT_LIKE("NOT LIKE"),
    NOT_LIKE_START_WITH("NOT LIKE"),
    NOT_LIKE_END_WITH("NOT LIKE"),
    LIKE("LIKE"),
    LIKE_START_WITH("LIKE"),
    LIKE_END_WITH("LIKE"),
    IN("IN"),
    NOT_IN("NOT IN"),
    IS_NULL("IS NULL"),
    IS_NOT_NULL("IS NOT NULL"),
    IS_TRUE("IS TRUE"),
    IS_FALSE("IS FALSE"),
    IS_NOT_TRUE("IS NOT TRUE"),
    IS_NOT_FALSE("IS NOT FALSE");

    private final String text;

    ComparissonOperator(String text) {
        this.text = text;
    }

}
