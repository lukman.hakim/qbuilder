package com.qbuilder.core;

import lombok.*;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SearchQuery {
    private List<SearchCriteria> searchCriterias;
    private SearchRsql searchRsql;
    private List<SearchFrom> searchFroms;
    private List<SearchSelect> searchSelects;
    private List<String> groupBys;
    private List<SortCase> sortCases;
    private SearchGroup searchGroup;
    private Sort sort;
    private String distinct;
    @Builder.Default
    private Boolean isDistinct = Boolean.FALSE;
    @Builder.Default
    private Boolean allLeftJoin = Boolean.FALSE;

    public SearchQuery add(SearchCriteria searchCriteria) {
        if (this.searchCriterias == null) {
            this.searchCriterias = new ArrayList<>();
        }
        this.searchCriterias.add(searchCriteria);

        return this;
    }

    public SearchQuery add(SearchSelect searchSelect) {
        if (this.searchSelects == null) {
            this.searchSelects = new ArrayList<>();
        }

        if (searchSelect.getSelectOperation() == SelectOperation.DISTINCT) {
            long totalDistinct = searchSelects.stream().filter(s -> s.getSelectOperation() == SelectOperation.DISTINCT).count();

            if (totalDistinct != 0) {
                throw new RuntimeException("SearchQuery utils Only support 1 Distinct, use group by instead");
            }

            isDistinct = true;
        }

        this.searchSelects.add(searchSelect);

        return this;
    }

    public SearchQuery rsql(String rsql) {
        this.searchRsql = SearchRsql.get(rsql);

        return this;
    }

    public SearchQuery add(SearchFrom searchFrom) {
        if (this.searchFroms == null) {
            this.searchFroms = new ArrayList<>();
        }

        this.searchFroms.add(searchFrom);

        return this;
    }

    public SearchQuery add(Sort sort) {
        return sort(sort);
    }

    public SearchQuery sort(Sort sort) {
        this.sort = sort;

        return this;
    }

    SearchQuery groupBy(String... attribute) {
        return groupBy(Arrays.asList(attribute));
    }

    SearchQuery groupBy(List<String> attributes) {
        if (this.groupBys == null) {
            this.groupBys = new ArrayList<>();
        }

        this.groupBys.addAll(attributes);

        return this;
    }

    public static SearchQuery empty() {
        return SearchQuery.builder().build();
    }

    public static SearchQuery of(SearchCriteria searchCriteria) {
        List<SearchCriteria> s = new ArrayList<>();
        s.add(searchCriteria);
        return SearchQuery.builder()
                .searchCriterias(s)
                .build();
    }

    public static SearchQuery of(Sort sort, SearchCriteria searchCriteria) {
        List<SearchCriteria> s = new ArrayList<>();
        s.add(searchCriteria);
        return SearchQuery.builder()
                .searchCriterias(s)
                .sort(sort)
                .build();
    }

    public static SearchQuery of(SearchCriteria... searchCriterias) {
        List<SearchCriteria> s = new ArrayList<>(Arrays.asList(searchCriterias));
        return SearchQuery.builder()
                .searchCriterias(s)
                .build();
    }

    public static SearchQuery of(Sort sort, SearchCriteria... searchCriterias) {
        List<SearchCriteria> s = new ArrayList<>(Arrays.asList(searchCriterias));
        return SearchQuery.builder()
                .searchCriterias(s)
                .sort(sort)
                .build();
    }

    public static SearchQuery of(String rsql) {
        return SearchQuery.builder()
                .searchRsql(SearchRsql.get(rsql))
                .build();
    }

    public static SearchQuery copy(SearchQuery source) {
        return SearchQuery.builder()
                .searchCriterias(source.getSearchCriterias() == null ? null : new ArrayList<>(source.getSearchCriterias()))
                .searchRsql(source.getSearchRsql() == null ? null :
                        SearchRsql.builder()
                                .rsql(source.getSearchRsql().getRsql())
                                .operators(source.getSearchRsql().getOperators())
                                .build())
                .searchFroms(source.getSearchFroms() == null ? null : new ArrayList<>(source.getSearchFroms()))
                .searchSelects(source.getSearchSelects() == null ? null : new ArrayList<>(source.getSearchSelects()))
                .groupBys(source.getGroupBys() == null ? null : new ArrayList<>(source.getGroupBys()))
                .sort(source.getSort())
                .distinct(source.getDistinct())
                .isDistinct(source.getIsDistinct())
                .sortCases(source.getSortCases() == null ? null : new ArrayList<>(source.getSortCases()))
                .searchGroup(source.getSearchGroup())
                .allLeftJoin(source.getAllLeftJoin())
                .build();
    }

    public static Pageable getOffsetBasedPageable(long offset, int limit, String sortBy, String direction) {
        return getPageable(offset, limit, sortBy, direction, Sort.NullHandling.NATIVE, true);
    }

    public static Pageable getOffsetBasedPageable(long offset, int limit, String sortBy, String direction, Sort.NullHandling nullHandling) {
        return getPageable(offset, limit, sortBy, direction, nullHandling, true);
    }

    public static Pageable getOffsetBasedPageable(long offset, int limit, List<String> sortBys, List<String> directions) {
        return getPageable(offset, limit, sortBys, directions, true);
    }

    public static Pageable getOffsetBasedPageable(long offset, int limit, List<String> sortBys, List<String> directions, List<Sort.NullHandling> nullHandlings) {
        return getPageable(offset, limit, sortBys, directions, nullHandlings, true);
    }

    public static Pageable getOffsetBasedPageable(long offset, int limit, List<Triple<String, String, Sort.NullHandling>> sortTriples) {
        return getPageable(offset, limit, sortTriples, true);
    }

    public static Pageable getPageable(int page, int size, String sortBy, String direction) {
        return getPageable(page, size, sortBy, direction, Sort.NullHandling.NATIVE, false);
    }

    public static Pageable getPageable(int page, int limit, String sortBy, String direction, Sort.NullHandling nullHandling) {
        return getPageable(page, limit, sortBy, direction, nullHandling, false);
    }

    public static Pageable getPageable(int page, int limit, List<String> sortBys, List<String> directions) {
        return getPageable(page, limit, sortBys, directions, false);
    }

    public static Pageable getPageable(int page, int limit, List<String> sortBys, List<String> directions, List<Sort.NullHandling> nullHandlings) {
        return getPageable(page, limit, sortBys, directions, nullHandlings, false);
    }

    public static Pageable getPageable(int page, int limit, List<Triple<String, String, Sort.NullHandling>> sortTriples) {
        return getPageable(page, limit, sortTriples, false);
    }

    public static Pageable getPageable(long offsetOrPage, int size, String sortBy, String direction, Sort.NullHandling nullHandling, boolean offsetBased) {
        Triple<String, String, Sort.NullHandling> sortTriple = Triple.of(sortBy, direction, nullHandling);
        return getPageable(offsetOrPage, size, Collections.singletonList(sortTriple), offsetBased);
    }

    public static Pageable getPageable(long offsetOrPage, int size, List<String> sortBys, List<String> directions, boolean offsetBased) {
        if (sortBys.size() != directions.size()) throw new RuntimeException("sort and direction size not valid!");
        List<Triple<String, String, Sort.NullHandling>> sortTriples = new ArrayList<>();
        for (int i = 0; i < sortBys.size(); i++) {
            String s = sortBys.get(i);
            String d = directions.get(i);
            Sort.NullHandling n = Sort.NullHandling.NATIVE;
            sortTriples.add(Triple.of(s, d, n));
        }

        return getPageable(offsetOrPage, size, sortTriples, offsetBased);
    }

    public static Pageable getPageable(long offsetOrPage, int size, List<String> sortBys, List<String> directions, List<Sort.NullHandling> nullHandling, boolean offsetBased) {
        if (sortBys.size() != directions.size()) throw new RuntimeException("sort and direction size not valid!");
        List<Triple<String, String, Sort.NullHandling>> sortTriples = new ArrayList<>();
        for (int i = 0; i < sortBys.size(); i++) {
            String s = sortBys.get(i);
            String d = directions.get(i);
            Sort.NullHandling n = nullHandling.get(i);
            sortTriples.add(Triple.of(s, d, n));
        }

        return getPageable(offsetOrPage, size, sortTriples, offsetBased);
    }

    public static Pageable getPageable(long offsetOrPage, int size, List<Triple<String, String, Sort.NullHandling>> sortTriples, boolean offsetBased) {
        Sort sort = Sort.unsorted();

        for (Triple<String, String, Sort.NullHandling> sortTriple : sortTriples) {
            String s = sortTriple.getLeft();
            String d = sortTriple.getMiddle();
            Sort.NullHandling n = sortTriple.getRight();

            Sort.Order order = d.equals("desc") ?
                    new Sort.Order(Sort.Direction.DESC, s).with(n) :
                    new Sort.Order(Sort.Direction.ASC, s).with(n);

            sort = sort.and(Sort.by(order));
        }

        return offsetBased ?
                getOffsetBasedPageable(offsetOrPage, size, sort) :
                getPageable((int) offsetOrPage, size, sort);
    }

    public static Pageable getPageable(int page, int size, Sort sort) {
        return PageRequest.of(page, size, sort);
    }

    public static Pageable getOffsetBasedPageable(long offset, int limit, Sort sort) {
        return new OffsetBasedPageRequest(offset, limit, sort);
    }

    private static int getPage(long offset, int limit) {
        return (int) Math.floor((double) offset / limit);
    }

    public SearchQuery add(SortCase... sortCase) {
        if (this.sortCases == null) {
            this.sortCases = new ArrayList<>();
        }

        sortCases.addAll(Arrays.asList(sortCase));

        return this;
    }

    public <E extends Enum<E>> void sortCase(String attribute, Class<E> enumClass) {
        E[] enumValues = enumClass.getEnumConstants();
        Arrays.sort(enumValues, Comparator.comparingInt(Enum::ordinal));

        for (E enumValue : enumValues) {
            this.add(SortCase.with(attribute, ComparissonOperator.EQUAL, enumValue));
        }
    }

    public SearchQuery add(SearchGroup searchGroup) {
        this.searchGroup = searchGroup;
        return this;
    }
}
