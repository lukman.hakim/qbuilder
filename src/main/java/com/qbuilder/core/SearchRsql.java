package com.qbuilder.core;

import lombok.*;
import org.apache.commons.lang3.StringUtils;

import java.util.Set;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SearchRsql {

    private Set<String> operators;
    private String rsql;

    public static SearchRsql get(String rsql) {
        if (StringUtils.isBlank(rsql)) return null;

        return SearchRsql.builder()
                .rsql(rsql)
                .operators(RsqlHelper.getOperators(rsql))
                .build();
    }
}