package com.qbuilder.core;

import lombok.Getter;

@Getter
public enum LogicalOperator {
    AND,
    OR

}
