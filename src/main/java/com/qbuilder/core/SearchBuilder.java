package com.qbuilder.core;

import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.criteria.JoinType;
import lombok.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.lang.annotation.Annotation;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SearchBuilder {
    private static final String SPACE = " ";
    private static final String BLANK = "";
    private static final String OPEN_WRAP = "(";
    private static final String CLOSE_WRAP = ")";
    private static final String PARAM_PREFIX = ":";
    private static final String PARAM_NAME = "param";
    private static final String DOT = ".";
    public static final String SORT_CASE_ALIAS = "SORT_CASE_ALIAS";

    @Builder.Default
    private Map<String, String> aliasMap = new HashMap<>();
    @Builder.Default
    private Map<String, String> joinMethodMap = new HashMap<>();
    @Builder.Default
    private Map<String, Object> parameters = new HashMap<>();
    @Builder.Default
    private int paramIndex = 1;

    private String query;
    private String rsql;
    private SearchQuery searchQuery;
    private Class<?> sourceClass;
    private boolean count;

    @Builder.Default
    private Sort sort = Sort.unsorted();
    private Pageable pageable;
    private String entityName;
    private String sourceIdAttribute;

    public static SearchBuilder query(SearchQuery searchQuery) {
        return query(searchQuery, null);
    }

    public static SearchBuilder query(Class<?> sourceClass, SearchQuery searchQuery) {
        return query(searchQuery, sourceClass);
    }

    public static SearchBuilder query(SearchQuery searchQuery, Class<?> sourceClass) {
        return SearchBuilder.builder()
                .searchQuery(searchQuery)
                .sourceClass(sourceClass)
                .build();
    }

    public SearchBuilder build() {
        return build(null);
    }

    public SearchBuilder build(Pageable pageable) {
        if (sourceClass == null) {
            throw new RuntimeException("sourceClass is null!");
        }

        this.entityName = EntityUtil.getEntityName(sourceClass);
        this.sourceIdAttribute = EntityUtil.getId(sourceClass);
        this.pageable = getPageable(pageable);

        if (StringUtils.isNotBlank(this.getQuery()) && !this.count) return this;

        if (this.searchQuery.getSearchRsql() == null || StringUtils.isBlank(this.searchQuery.getSearchRsql().getRsql())) {
            this.searchQuery.setSearchRsql(null);
        } else {
            this.rsql = this.searchQuery.getSearchRsql().getRsql();
        }

        if (CollectionUtils.isEmpty(searchQuery.getSearchSelects())) {
            searchQuery.add(SearchSelect.distinct(entityName));
        }

        StringBuilder queryBuilder = new StringBuilder();
        generateQuery(queryBuilder);
        this.query = queryBuilder.toString();

        return this;
    }

    private void generateQuery(StringBuilder queryBuilder) {
        mapAliases();
        whereByCriteria(queryBuilder);
        whereByRsql(queryBuilder);
        fromQuery(queryBuilder);
        selectQuery(queryBuilder);
        modifyGroupQuery(queryBuilder);
        groupByQuery(queryBuilder);

        // jika jenis count query, skip odering
        if (!this.count) {
            orderQuery(queryBuilder);
        }
    }

    private void processCriteria(StringBuilder queryBuilder, SearchCriteria searchCriteria, boolean appendedLogical) {
        List<SearchCriteria> searchCriterias = searchCriteria.getSearchCriterias();

        if (searchCriterias != null) {
            boolean appendedLogic = false;

            searchCriterias.removeIf(sc -> isInvalidValue(sc.getComparissonOperator(), sc.getValues(), sc.getSearchCriterias()));

            if (searchCriterias.isEmpty()) {
                if (appendedLogical) {
                    deleteUnnecessaryAnd(queryBuilder);
                }
                return;
            }

            if (searchCriterias.size() > 1) {
                queryBuilder.append(OPEN_WRAP);
            }

            int i = 1;

            for (SearchCriteria sc : searchCriterias) {
                if (!CollectionUtils.isEmpty(sc.getSearchCriterias())) {
                    processCriteria(queryBuilder, sc, appendedLogic);
                }

                addCriteria(queryBuilder, sc);

                if (i < searchCriterias.size()) {
                    queryBuilder.append(SPACE).append(searchCriteria.getLogicalOperator()).append(SPACE);
                    appendedLogic = true;
                }

                i++;
            }

            if (searchCriterias.size() > 1) {
                queryBuilder.append(CLOSE_WRAP);
            }
        } else {
            addCriteria(queryBuilder, searchCriteria);
        }

    }

    public void addCriteria(StringBuilder queryBuilder, SearchCriteria searchCriteria) {
        if (isInvalidValue(searchCriteria.getComparissonOperator(), searchCriteria.getValues())) return;
        boolean isMultiValue = isMultiValue(searchCriteria.getComparissonOperator());
        boolean isNoValue = isNoValue(searchCriteria.getComparissonOperator());
        boolean isLike = isLike(searchCriteria.getComparissonOperator());
        boolean isLikeStartWith = isLikeStartWith(searchCriteria.getComparissonOperator());
        boolean isLikeEndWith = isLikeEndWith(searchCriteria.getComparissonOperator());
        boolean isBetween = isBetween(searchCriteria.getComparissonOperator());
        String attribute = getMaskedAttribute(searchCriteria.getAttribute());

        // query
        // write attribute
        if (!searchCriteria.isCaseSensitive()) {
            queryBuilder.append("LOWER(");
        }
        if (searchCriteria.isToChar()) {
            queryBuilder.append("CAST(").append(attribute).append(SPACE).append("AS string").append(")");
        } else {
            queryBuilder.append(attribute);
        }
        if (!searchCriteria.isCaseSensitive()) {
            queryBuilder.append(")");
        }

        // write operator
        queryBuilder.append(SPACE).append(searchCriteria.getComparissonOperator().getText()).append(SPACE);

        // write value
        if (isNoValue) return;

        // parameters
        if (isBetween) {
            queryBuilder.append(PARAM_PREFIX).append(PARAM_NAME).append(paramIndex);
            parameters.put(PARAM_NAME + paramIndex, searchCriteria.getValues().get(0));
            paramIndex++;
            queryBuilder.append(" AND ");
            queryBuilder.append(PARAM_PREFIX).append(PARAM_NAME).append(paramIndex);
            parameters.put(PARAM_NAME + paramIndex, searchCriteria.getValues().get(1));
            paramIndex++;
        } else {
            if (isLike) {
                queryBuilder.append("CONCAT('%',");
                appendValue(searchCriteria, queryBuilder);
                queryBuilder.append(", '%')");
            } else if (isLikeEndWith) {
                queryBuilder.append("CONCAT('%',");
                appendValue(searchCriteria, queryBuilder);
                queryBuilder.append(")");
            } else if (isLikeStartWith) {
                queryBuilder.append("CONCAT(");
                appendValue(searchCriteria, queryBuilder);
                queryBuilder.append(", '%')");
            } else {
                appendValue(searchCriteria, queryBuilder);
            }

            parameters.put(PARAM_NAME + paramIndex, isMultiValue ? searchCriteria.getValues() : searchCriteria.getValues().get(0));

            paramIndex++;
        }

    }

    private void appendValue(SearchCriteria searchCriteria, StringBuilder queryBuilder) {
        if (!searchCriteria.isCaseSensitive()) {
            queryBuilder.append("LOWER(");
        }
        queryBuilder.append(PARAM_PREFIX).append(PARAM_NAME).append(paramIndex);
        if (!searchCriteria.isCaseSensitive()) {
            queryBuilder.append(")");
        }
    }

    private boolean isMultiValue(ComparissonOperator comparissonOperator) {
        List<ComparissonOperator> comparissonOperators = Arrays.asList(
                ComparissonOperator.IN,
                ComparissonOperator.NOT_IN);
        return comparissonOperators.contains(comparissonOperator);
    }

    private boolean isNoValue(ComparissonOperator comparissonOperator) {
        List<ComparissonOperator> comparissonOperators = Arrays.asList(
                ComparissonOperator.IS_NOT_NULL,
                ComparissonOperator.IS_NULL,
                ComparissonOperator.IS_TRUE,
                ComparissonOperator.IS_FALSE,
                ComparissonOperator.IS_NOT_TRUE,
                ComparissonOperator.IS_NOT_FALSE);
        return comparissonOperators.contains(comparissonOperator);
    }

    private boolean isLike(ComparissonOperator comparissonOperator) {
        List<ComparissonOperator> comparissonOperators = Arrays.asList(
                ComparissonOperator.LIKE,
                ComparissonOperator.NOT_LIKE);
        return comparissonOperators.contains(comparissonOperator);
    }

    private boolean isLikeStartWith(ComparissonOperator comparissonOperator) {
        List<ComparissonOperator> comparissonOperators = Arrays.asList(
                ComparissonOperator.LIKE_START_WITH,
                ComparissonOperator.NOT_LIKE_START_WITH);
        return comparissonOperators.contains(comparissonOperator);
    }

    private boolean isLikeEndWith(ComparissonOperator comparissonOperator) {
        List<ComparissonOperator> comparissonOperators = Arrays.asList(
                ComparissonOperator.LIKE_END_WITH,
                ComparissonOperator.NOT_LIKE_END_WITH);
        return comparissonOperators.contains(comparissonOperator);
    }

    private boolean isBetween(ComparissonOperator comparissonOperator) {
        List<ComparissonOperator> comparissonOperators = Arrays.asList(
                ComparissonOperator.BETWEEN,
                ComparissonOperator.NOT_BETWEEN);
        return comparissonOperators.contains(comparissonOperator);
    }

    private boolean isInvalidValue(ComparissonOperator comparissonOperator, List<?> values) {
        return !isNoValue(comparissonOperator) && (CollectionUtils.isEmpty(values) || values.stream().anyMatch(v -> v != null && String.valueOf(v).isBlank()));
    }

    private boolean isInvalidValue(ComparissonOperator comparissonOperator, List<?> values, List<SearchCriteria> searchCriterias) {
        return isInvalidValue(comparissonOperator, values) && CollectionUtils.isEmpty(searchCriterias);
    }

    private void deleteUnnecessaryAnd(StringBuilder queryBuilder) {
        queryBuilder.delete(queryBuilder.length() - 5, queryBuilder.length());
    }

    private boolean isNeedWrap(SelectOperation selectOperation) {
        return Arrays.asList(SelectOperation.MAX, SelectOperation.MIN, SelectOperation.AVG, SelectOperation.SUM).contains(selectOperation);
    }

    private void fromQuery(StringBuilder queryBuilder) {
        aliasMap = sortByNumberOfDots(aliasMap);
        StringBuilder prependBuilder = new StringBuilder();
        prependBuilder.append("FROM").append(SPACE).append(sourceClass.getCanonicalName()).append(SPACE).append(entityName);
        for (String key : aliasMap.keySet()) {
            List<String> attributes = Arrays.asList(key.split("\\."));
            String join = aliasMap.get(key);
            if (attributes.size() == 1) {
                String joinMethod = joinMethodMap.get(join);
                joinMethod = !StringUtils.isBlank(joinMethod) ? joinMethod : getJoinMethod(entityName + DOT + key, sourceClass);
                prependBuilder.append(SPACE).append(joinMethod).append(SPACE).append(entityName).append(DOT).append(key).append(SPACE).append(join);
            } else {
                String joinPath = String.join(DOT, attributes.subList(0, attributes.size() - 1));
                String entity = attributes.get(attributes.size() - 1);
                String attributePath = String.join(DOT, attributes);
                String joinAlias = aliasMap.get(joinPath);
                String attributePathAlias = aliasMap.get(attributePath);
                prependBuilder.append(SPACE).append(joinMethodMap.get(attributePathAlias)).append(SPACE).append(joinAlias).append(DOT).append(entity).append(SPACE).append(join);
            }
        }

        if (!queryBuilder.isEmpty()) {
            prependBuilder.append(SPACE).append("WHERE").append(SPACE);
        }

        queryBuilder.insert(0, prependBuilder);
    }

    private void selectQueryDistinct(StringBuilder queryBuilder) {
        StringBuilder prependBuilder = new StringBuilder();
        if (this.count) {
            prependBuilder.append("SELECT COUNT(DISTINCT ").append(entityName).append(DOT).append(sourceIdAttribute).append(")").append(SPACE);
        }

        queryBuilder.insert(0, prependBuilder);
    }

    private void selectQuery(StringBuilder queryBuilder) {
        if (CollectionUtils.isEmpty(searchQuery.getSearchSelects()) || this.count) {
            selectQueryDistinct(queryBuilder);
            return;
        }

        List<String> selects = new ArrayList<>();
        StringBuilder prependBuilder = new StringBuilder();
        prependBuilder.append("SELECT");

        if (CollectionUtils.isEmpty(searchQuery.getSearchSelects())) {
            prependBuilder.append(SPACE).append(entityName).append(SPACE);
        }

        for (SearchSelect searchSelect : searchQuery.getSearchSelects()) {
            StringBuilder builder = new StringBuilder();
            SelectOperation operation = searchSelect.getSelectOperation();
            String selectAttribute = operation == SelectOperation.DISTINCT && StringUtils.isEmpty(searchSelect.getAttribute()) ? entityName : searchSelect.getAttribute();
            String attribute = operation == SelectOperation.DISTINCT && selectAttribute.equalsIgnoreCase(entityName) ? entityName : getMaskedAttribute(selectAttribute);
            boolean needWrap = isNeedWrap(operation);

            builder.append(SPACE).append(operation == null ? BLANK : operation).append(operation == SelectOperation.DISTINCT ? SPACE : BLANK);
            builder.append(needWrap ? "(" : BLANK);
            builder.append(attribute);
            builder.append(needWrap ? ")" : BLANK);

            if (StringUtils.isNotBlank(searchSelect.getAlias())) {
                builder.append(SPACE).append("AS").append(SPACE).append(searchSelect.getAlias());
            }

            selects.add(builder.toString());
        }

        prependBuilder.append(String.join(",", selects)).append(SPACE);

        queryBuilder.insert(0, prependBuilder);
    }

    private void groupByQuery(StringBuilder queryBuilder) {
        if (CollectionUtils.isEmpty(searchQuery.getGroupBys())) return;

        List<String> groups = new ArrayList<>();
        StringBuilder prependBuilder = new StringBuilder();
        prependBuilder.append("GROUP BY").append(SPACE);

        for (String groupBy : searchQuery.getGroupBys()) {
            groups.add(getMaskedAttribute(groupBy));
        }

        prependBuilder.append(String.join(", ", groups));

        queryBuilder.append(SPACE).append(prependBuilder);
    }

    private void modifyGroupQuery(StringBuilder queryBuilder) {
        if (searchQuery.getSearchGroup() == null) return;

        Sort.Order order = searchQuery.getSearchGroup().getSort().stream().findFirst().orElse(null);
        String selectOrder = order == null ? sourceIdAttribute : order.getProperty();
        String direction = order != null && order.getDirection() == Sort.Direction.DESC ? "MAX" : "MIN";
        String entityGroupName = entityName + "_group";
        List<String> queryGroupWhereAdds = new ArrayList<>();
        for (String attribute : searchQuery.getSearchGroup().getAttributes()) {
            queryGroupWhereAdds.add(entityGroupName + DOT + attribute + " = " + entityName + DOT + attribute);
        }

        String queryGroup = queryBuilder.toString();
        queryGroup = queryGroup.replace(entityName, entityGroupName);

        for (String alias : aliasMap.values()) {
            queryGroup = queryGroup.replace(alias, alias + "_group");
        }

        String[] queryGroupSplit = queryGroup.split("(?i)(FROM|WHERE|GROUP BY)");

        String queryGroupSelect = "SELECT " + direction + "(" + entityGroupName + DOT + selectOrder + ") ";
        String queryGroupFrom = "FROM " + queryGroupSplit[1] + " ";
        String queryGroupWhere = "WHERE " + queryGroupSplit[2] + " ";
        queryGroupWhere += " AND (";
        queryGroupWhere += String.join(" AND ", queryGroupWhereAdds);
        queryGroupWhere += ")";

        queryGroup = queryGroupSelect + queryGroupFrom + queryGroupWhere;

        String queryString = queryBuilder.toString();
        String[] queryStringSplit = queryString.split("(?i)(FROM|WHERE|GROUP BY)");
        String queryStringWhere = "WHERE " + entityName + DOT + selectOrder + " = (" + queryGroup + ")";
        String finalQueryString = queryStringSplit[0] + SPACE;
        finalQueryString += "FROM " + queryStringSplit[1] + SPACE;
        finalQueryString += queryStringWhere + SPACE;

        queryBuilder.setLength(0);
        queryBuilder.append(finalQueryString);
    }

    public String getMaskedAttribute(String attribute) {
        List<String> attributes = Arrays.asList(attribute.split("\\."));
        String attributeJoin = attributes.size() == 1 || attributes.get(0).equalsIgnoreCase(entityName) || StringUtils.isBlank(attribute) ?
                entityName :
                aliasMap.get(String.join(DOT, attributes.subList(0, attributes.size() - 1)));

        if (attributeJoin == null) return null;

        attributeJoin = attributeJoin + DOT + attributes.get(attributes.size() - 1);
        return attributeJoin;
    }

    public void whereByRsql(StringBuilder queryBuilder) {
        if (searchQuery.getSearchRsql() == null) return;

        rsql = RsqlHelper.toJpql(this);

        for (String operator : searchQuery.getSearchRsql().getOperators()) {
            String regex = RsqlHelper.getRegex(operator);
            Pattern pattern = Pattern.compile(regex);

            // use original rsql (SearchRSQL.rsql) to fetch regex matcher
            Matcher matcher = pattern.matcher(searchQuery.getSearchRsql().getRsql());

            while (matcher.find()) {
                String attribute = matcher.group(1);
                String maskedAttribute = getMaskedAttribute(attribute);

                if (maskedAttribute == null) continue;

                // Update regex to match attributes inside any function like LOWER(), UPPER(), or with no function, to update add after UPPER
                String rgx = "(?<=LOWER\\(|UPPER\\()\\b" + attribute.replace(".", "\\.") + "\\b(?=\\))|(?<!\\.)\\b" + attribute.replace(".", "\\.") + "\\b(?=\\s*(=|<|>|!|BETWEEN|IN|NOT IN|LIKE|NOT LIKE|IS NULL|IS NOT NULL|IS TRUE|IS FALSE|IS NOT TRUE|IS NOT FALSE))";
                rsql = rsql.replaceAll(rgx, maskedAttribute);
            }
        }

        if (!CollectionUtils.isEmpty(searchQuery.getSearchCriterias())) {
            queryBuilder.append(" AND (");
        }
        queryBuilder.append(rsql);

        if (!CollectionUtils.isEmpty(searchQuery.getSearchCriterias())) {
            queryBuilder.append(")");
        }
    }

    private void joinMap(SearchCriteria searchCriteria) {
        if (!CollectionUtils.isEmpty(searchCriteria.getSearchCriterias())) {
            searchCriteria.getSearchCriterias().forEach(this::joinMap);
        }
        joinMap(searchCriteria.getAttribute(), searchCriteria.isEmbeddedAttribute());
    }

    private void joinMap(SearchSelect searchSelect) {
        joinMap(searchSelect.getAttribute(), searchSelect.isEmbeddedAttribute());

    }

    private void joinMap(SortCase sortCase) {
        joinMap(sortCase.getAttribute(), false);
    }

    private void joinMap(String attribute) {
        joinMap(attribute, false);
    }

    private void joinMap(String attribute, boolean embeddedAttribute) {
        if (StringUtils.isBlank(attribute)) return;

        String[] attributes = attribute.split("\\.");

        if (embeddedAttribute || attributes.length == 1 || attributes[0].equals(entityName)) return;

        for (int i = attributes.length - 2; i >= 0; i--) {
            String attr = String.join(DOT, new ArrayList<>(Arrays.asList(attributes).subList(0, i + 1)));
            String alias = attributes[i] + "_" + RandomStringUtils.randomAlphanumeric(2);

            if (!aliasMap.containsKey(attr)) {
                // seek join type from search from
                JoinType joinType = searchQuery.getAllLeftJoin() ? JoinType.LEFT : null;
                joinType = joinType != null ? joinType : CollectionUtils.isEmpty(searchQuery.getSearchFroms()) ? null : searchQuery.getSearchFroms().stream()
                        .filter(a -> a.getFrom().equalsIgnoreCase(attr))
                        .map(SearchFrom::getJoinType)
                        .findFirst().orElse(null);

                aliasMap.put(attr, alias);
                joinMethodMap.put(alias, joinType != null ? SearchFrom.joinType(joinType) : getJoinMethod(attr, sourceClass));
            }
        }
    }

    private void joinMap(String attribute, JoinType joinType) {
        if (StringUtils.isBlank(attribute) || joinType == null) return;
        String[] attributes = attribute.split("\\.");
        String alias = attributes.length == 1 ? attribute : attributes[attributes.length - 1];
        alias = alias + "_" + RandomStringUtils.randomAlphanumeric(2);

        if (!aliasMap.containsKey(attribute)) {
            aliasMap.put(attribute, alias);
            joinMethodMap.put(alias, SearchFrom.joinType(joinType));
        }
    }

    private Map<String, String> sortByNumberOfDots(Map<String, String> map) {
        List<Map.Entry<String, String>> list = new LinkedList<>(map.entrySet());
        list.sort(Comparator.comparingInt(entry -> countDots(entry.getKey())));

        Map<String, String> sortedMap = new LinkedHashMap<>();
        for (Map.Entry<String, String> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }

    private int countDots(String key) {
        return key.length() - key.replace(DOT, BLANK).length();
    }

    private Pageable getPageable(Pageable pageable) {
        PageRequest p;

        if (pageable == null) {
            p = PageRequest.of(0, Integer.MAX_VALUE);
        } else if (pageable.isUnpaged()) {
            p = PageRequest.of(0, Integer.MAX_VALUE, pageable.getSort());
        } else {
            p = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), pageable.getSort());
        }

        if (this.searchQuery.getSort() != null) {
            p = p.withSort(this.searchQuery.getSort());
        }

        if (!p.getSort().isEmpty()) {
            Sort sort = Sort.unsorted();
            for (Sort.Order o : p.getSort()) {
                joinMap(o.getProperty());
                Sort.Order order = new Sort.Order(o.getDirection(), getMaskedAttribute(o.getProperty())).with(o.getNullHandling());
                sort = sort.and(Sort.by(order));

            }
            p = p.withSort(sort);
        }

        return p;
    }

    private void orderQuery(StringBuilder queryBuilder) {
        String sortCase = querySortCase();
        sortCase = StringUtils.isEmpty(sortCase) ? "" : sortCase + ", ";
        sort = pageable.getSort();
        if (sort.isSorted()) {
            String sortBy = sort.stream().map(column -> {
                String direction = column.getDirection() == Sort.Direction.ASC ? "ASC" : "DESC";
                String nullHandling;
                switch (column.getNullHandling()) {
                    case NULLS_FIRST:
                        nullHandling = "NULLS FIRST";
                        break;
                    case NULLS_LAST:
                        nullHandling = "NULLS LAST";
                        break;
                    default:
                        nullHandling = "";
                        break;
                }

                return String.join(" ", column.getProperty(), direction, nullHandling);
            }).collect(Collectors.joining(", "));
            queryBuilder.append(" ORDER BY ").append(sortCase).append(sortBy);
        }
    }

    private void mapAliases() {
        if (!CollectionUtils.isEmpty(searchQuery.getSearchFroms())) {
            searchQuery.getSearchFroms().forEach(f -> joinMap(f.getFrom(), f.getJoinType()));
        }

        if (!CollectionUtils.isEmpty(searchQuery.getSearchCriterias())) {
            searchQuery.getSearchCriterias().forEach(this::joinMap);
        }
        //implement search from join map

        if (!CollectionUtils.isEmpty(searchQuery.getSearchSelects())) {
            searchQuery.getSearchSelects().forEach(this::joinMap);
        }

        if (searchQuery.getSearchRsql() != null) {
            RsqlHelper.getAttributes(searchQuery.getSearchRsql()).forEach(this::joinMap);
        }

        if (!CollectionUtils.isEmpty(searchQuery.getGroupBys())) {
            searchQuery.getGroupBys().forEach(this::joinMap);
        }

        if (!CollectionUtils.isEmpty(searchQuery.getSortCases())) {
            searchQuery.getSortCases().forEach(this::joinMap);
        }

        // Pageable or SearchQuery sort already mapped at getPageable method
    }

    private void whereByCriteria(StringBuilder queryBuilder) {
        if (CollectionUtils.isEmpty(searchQuery.getSearchCriterias())) return;

        int i = 1;
        for (SearchCriteria searchCriteria : searchQuery.getSearchCriterias()) {
            String recentQuery = queryBuilder.toString();
            processCriteria(queryBuilder, searchCriteria, false);

            if (recentQuery.length() < queryBuilder.toString().length() && i < searchQuery.getSearchCriterias().size()) {
                queryBuilder.append(SPACE).append(LogicalOperator.AND).append(SPACE);
            }

            i++;
        }

        if (queryBuilder.length() > 5 && queryBuilder.substring(queryBuilder.length() - 5, queryBuilder.length()).contains(LogicalOperator.AND.toString())) {
            deleteUnnecessaryAnd(queryBuilder);
        }
    }

    private String getJoinMethod(String attributePath, Class<?> sourceClass) {
        String entityName = this.entityName != null ? this.entityName : EntityUtil.getEntityName(sourceClass);

        if (attributePath.split("\\.").length == 1) {
            attributePath = entityName + DOT + attributePath;
        }

        String attributeStart = attributePath.split("\\.")[0];

        if (!attributeStart.equals(entityName)) return "LEFT JOIN";

        String method = "JOIN";
        Annotation[] annotations = EntityUtil.getAnnotationsByAttributePath(attributePath, sourceClass);

        if (annotations == null) return method;

        for (Annotation annotation : annotations) {
            if (annotation.annotationType().equals(ManyToMany.class) || annotation.annotationType().equals(OneToMany.class)) {
                return "LEFT JOIN";
            }

            if (annotation.annotationType().equals(OneToOne.class)) {
                OneToOne oneToOne = (OneToOne) annotation;
                if (StringUtils.isNotBlank(oneToOne.mappedBy())) {
                    return "LEFT JOIN";
                }
            }

            if (annotation.annotationType().equals(JoinColumn.class)) {
                JoinColumn joinColumn = (JoinColumn) annotation;
                if (joinColumn.nullable()) {
                    return "LEFT JOIN";
                }
            }
        }

        return method;
    }

    private String querySortCase() {
        List<SortCase> sortCases = searchQuery.getSortCases();
        StringBuilder sortCaseBuilder = new StringBuilder();

        if (!CollectionUtils.isEmpty(sortCases)) {
            sortCaseBuilder.append("CASE ");

            int i = 1;
            for (SortCase sortCase : sortCases) {
                if (isInvalidValue(sortCase.getComparissonOperator(), sortCase.getValues())) continue;
                boolean isMultiValue = isMultiValue(sortCase.getComparissonOperator());
                boolean isNoValue = isNoValue(sortCase.getComparissonOperator());
                boolean isLike = isLike(sortCase.getComparissonOperator());
                boolean isLikeStartWith = isLikeStartWith(sortCase.getComparissonOperator());
                boolean isLikeEndWith = isLikeEndWith(sortCase.getComparissonOperator());
                boolean isBetween = isBetween(sortCase.getComparissonOperator());
                String attribute = getMaskedAttribute(sortCase.getAttribute());

                // query
                // write attribute
                sortCaseBuilder.append("WHEN ").append(attribute);

                // write operator
                sortCaseBuilder.append(SPACE).append(sortCase.getComparissonOperator().getText()).append(SPACE);

                // write value
                if (isNoValue) {
                    sortCaseBuilder.append("THEN ").append(i++).append(SPACE);
                } else {
                    // parameters
                    if (isBetween) {
                        sortCaseBuilder.append(PARAM_PREFIX).append(PARAM_NAME).append(paramIndex);
                        parameters.put(PARAM_NAME + paramIndex, sortCase.getValues().get(0));
                        paramIndex++;
                        sortCaseBuilder.append(" AND ");
                        sortCaseBuilder.append(PARAM_PREFIX).append(PARAM_NAME).append(paramIndex);
                        parameters.put(PARAM_NAME + paramIndex, sortCase.getValues().get(1));
                        paramIndex++;
                    } else {
                        if (isLike) {
                            sortCaseBuilder.append("CONCAT('%',");
                            sortCaseBuilder.append(PARAM_PREFIX).append(PARAM_NAME).append(paramIndex);
                            sortCaseBuilder.append(", '%')");
                        } else if (isLikeEndWith) {
                            sortCaseBuilder.append("CONCAT('%',");
                            sortCaseBuilder.append(PARAM_PREFIX).append(PARAM_NAME).append(paramIndex);
                            sortCaseBuilder.append(")");
                        } else if (isLikeStartWith) {
                            sortCaseBuilder.append("CONCAT(");
                            sortCaseBuilder.append(PARAM_PREFIX).append(PARAM_NAME).append(paramIndex);
                            sortCaseBuilder.append(", '%')");
                        } else {
                            sortCaseBuilder.append(PARAM_PREFIX).append(PARAM_NAME).append(paramIndex);
                        }

                        parameters.put(PARAM_NAME + paramIndex, isMultiValue ? sortCase.getValues() : sortCase.getValues().get(0));

                        paramIndex++;
                    }

                    sortCaseBuilder.append(" THEN ").append(i++).append(SPACE);
                }
            }

            sortCaseBuilder.append("ELSE ").append(i).append(SPACE);
            sortCaseBuilder.append("END");
        }

        return sortCaseBuilder.toString();
    }
}