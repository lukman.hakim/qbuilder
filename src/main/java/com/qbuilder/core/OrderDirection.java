package com.qbuilder.core;

import lombok.Getter;

@Getter
public enum OrderDirection {
    ASC,
    DESC

}
