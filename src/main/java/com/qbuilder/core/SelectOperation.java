package com.qbuilder.core;

import lombok.Getter;

@Getter
public enum SelectOperation {
    SUM("SUM"),
    MAX("MAX"),
    MIN("MIN"),
    AVG("AVG"),
    DISTINCT("DISTINCT");

    private final String text;

    SelectOperation(String text) {
        this.text = text;
    }

}
